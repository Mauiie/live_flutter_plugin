## 1.0.0
Initialize the project, encapsulate the shuttle Live SDK, based on Android SDK and IOS SDKs

## 1.0.1
* Android fixed bug of V2TXLivePusher not active with V2TXLiveMode.v2TXLiveModeRTMP

## 1.0.2
Fixed some bugs.

## 1.0.3
Fixed some bugs.