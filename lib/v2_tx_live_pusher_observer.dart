
///
/// 腾讯云直播推流的回调通知。<br/>
/// V2TXLivePusher 的一些回调事件，包括推流器状态，推流音量，统计信息，警告以及错误信息。
///

enum V2TXLivePusherListenerType {
  /// 错误回调，表示 SDK 不可恢复的错误，一定要监听并分情况给用户适当的界面提示
  onError,

  /// 警告回调，用于告知您一些非严重性问题，例如出现卡顿或者可恢复的解码失败。
  onWarning,

  /// 首帧音频采集完成的回调通知
  onCaptureFirstAudioFrame,

  /// 首帧视频采集完成的回调通知
  onCaptureFirstVideoFrame,

  /// 麦克风采集音量值回调
  onMicrophoneVolumeUpdate,

  /// 推流器连接状态回调通知
  onPushStatusUpdate,

  /// 直播推流器统计数据回调
  onStatisticsUpdate,

  /// 截图回调
  onSnapshotComplete,

  /// 自定义视频处理回调
  onProcessVideoFrame,

  /// SDK 内部的 OpenGL 环境的销毁通知
  onGLContextDestroyed,

  /// 设置云端的混流转码参数的回调
  onSetMixTranscodingConfig,

  /// 当屏幕分享开始时，SDK 会通过此回调通知
  onScreenCaptureStarted,

  /// 当屏幕分享停止时，SDK 会通过此回调通知
  onScreenCaptureStopped,

  /// 音频开始播放
  onMusicObserverStart,

  /// 音频播放中
  onMusicObserverPlayProgress,

  /// 音频播放结束
  onMusicObserverComplete,
}

typedef V2TXLivePusherObserver<P> = void Function(V2TXLivePusherListenerType type, P? params);

