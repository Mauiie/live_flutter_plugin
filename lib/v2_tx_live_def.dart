// ignore_for_file: constant_identifier_names

import 'dart:typed_data';

class V2TXLiveRenderView {
  static const String renderViewType = 'v2_live_view_factory';
}

/// 音乐和人声设置接口参数
class AudioMusicParam {
  /// 【字段含义】音乐 ID
  /// 【特殊说明】SDK 允许播放多路音乐，因此需要音乐 ID 进行标记，用于控制音乐的开始、停止、音量等
  int id;

  /// 【字段含义】音乐文件的绝对路径
  String path;

  /// 【字段含义】音乐循环播放的次数
  /// 【推荐取值】取值范围为0 - 任意正整数，默认值：0。0表示播放音乐一次；1表示播放音乐两次；以此类推
  int loopCount;

  /// 【字段含义】是否将音乐传到远端
  /// 【推荐取值】YES：音乐在本地播放的同时，会上行至云端，因此远端用户也能听到该音乐；NO：音乐不会上行至云端，因此只能在本地听到该音乐。默认值：NO
  bool publish;

  /// 【字段含义】播放的是否为短音乐文件
  /// 【推荐取值】YES：需要重复播放的短音乐文件；NO：正常的音乐文件。默认值：NO
  bool isShortFile;

  /// 【字段含义】音乐开始播放时间点，单位毫秒
  int startTimeMS;

  /// 【字段含义】音乐结束播放时间点，单位毫秒，0或者-1表示播放至文件结尾。
  int endTimeMS;

  AudioMusicParam(
      {required this.path,
      required this.id,
      this.loopCount = 0,
      this.publish = false,
      this.isShortFile = false,
      this.startTimeMS = 0,
      this.endTimeMS = -1});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['path'] = path;
    data['id'] = id;
    data['loopCount'] = loopCount;
    data['publish'] = publish;
    data['isShortFile'] = isShortFile;
    data['startTimeMS'] = startTimeMS;
    data['endTimeMS'] = endTimeMS;
    return data;
  }
}

/// 支持协议
enum V2TXLiveMode {
  /// 支持协议: RTMP
  v2TXLiveModeRTMP,
  /// 支持协议: TRTC
  v2TXLiveModeRTC,
}

/// 视频分辨率
enum V2TXLiveVideoResolution {
  /// 分辨率 160*160，码率范围：100Kbps ~ 150Kbps，帧率：15fps
  v2TXLiveVideoResolution160x160,

  /// 分辨率 270*270，码率范围：200Kbps ~ 300Kbps，帧率：15fps
  v2TXLiveVideoResolution270x270,

  /// 分辨率 480*480，码率范围：350Kbps ~ 525Kbps，帧率：15fps
  v2TXLiveVideoResolution480x480,

  /// 分辨率 320*240，码率范围：250Kbps ~ 375Kbps，帧率：15fps
  v2TXLiveVideoResolution320x240,

  /// 分辨率 480*360，码率范围：400Kbps ~ 600Kbps，帧率：15fps
  v2TXLiveVideoResolution480x360,

  /// 分辨率 640*480，码率范围：600Kbps ~ 900Kbps，帧率：15fps
  v2TXLiveVideoResolution640x480,

  /// 分辨率 320*180，码率范围：250Kbps ~ 400Kbps，帧率：15fps
  v2TXLiveVideoResolution320x180,

  /// 分辨率 480*270，码率范围：350Kbps ~ 550Kbps，帧率：15fps
  v2TXLiveVideoResolution480x270,

  /// 分辨率 640*360，码率范围：500Kbps ~ 900Kbps，帧率：15fps
  v2TXLiveVideoResolution640x360,

  /// 分辨率 960*540，码率范围：800Kbps ~ 1500Kbps，帧率：15fps
  v2TXLiveVideoResolution960x540,

  /// 分辨率 1280*720，码率范围：1000Kbps ~ 1800Kbps，帧率：15fps
  v2TXLiveVideoResolution1280x720,

  /// 分辨率 1920*1080，码率范围：2500Kbps ~ 3000Kbps，帧率：15fps
  v2TXLiveVideoResolution1920x1080,
}

/// 视频宽高比模式
/// note
/// - 横屏模式下的分辨率: V2TXLiveVideoResolution640_360 + V2TXLiveVideoResolutionModeLandscape = 640x360
/// - 竖屏模式下的分辨率: V2TXLiveVideoResolution640_360 + V2TXLiveVideoResolutionModePortrait = 360x640
enum V2TXLiveVideoResolutionMode {
  /// 横屏模式
  v2TXLiveVideoResolutionModeLandscape,

  /// 竖屏模式
  v2TXLiveVideoResolutionModePortrait,
}

class V2TXLiveVideoEncoderParam {
  ///【字段含义】 视频分辨率
  ///【特别说明】如需使用竖屏分辨率，请指定 videoResolutionMode 为 Portrait，例如： 640 × 360 + Portrait = 360 × 640。
  ///【推荐取值】
  /// - 桌面平台（Win + Mac）：建议选择 640 × 360 及以上分辨率，videoResolutionMode 选择 Landscape，即横屏分辨率。
  V2TXLiveVideoResolution videoResolution;

  ///【字段含义】分辨率模式（横屏分辨率 or 竖屏分辨率）
  ///【推荐取值】桌面平台（Windows、Mac）建议选择 Landscape。
  ///【特别说明】如需使用竖屏分辨率，请指定 resMode 为 Portrait，例如： 640 × 360 + Portrait = 360 × 640。
  V2TXLiveVideoResolutionMode videoResolutionMode;

  ///【字段含义】视频采集帧率
  ///【推荐取值】15fps 或 20fps。5fps 以下，卡顿感明显。10fps 以下，会有轻微卡顿感。20fps 以上，会浪费带宽（电影的帧率为 24fps）。
  int videoFps;

  ///【字段含义】目标视频码率，SDK 会按照目标码率进行编码，只有在弱网络环境下才会主动降低视频码率。
  ///【推荐取值】请参考 V2TXLiveVideoResolution 在各档位注释的最佳码率，也可以在此基础上适当调高。
  ///           比如：V2TXLiveVideoResolution1280x720 对应 1200kbps 的目标码率，您也可以设置为 1500kbps 用来获得更好的观感清晰度。
  ///【特别说明】您可以通过同时设置 videoBitrate 和 minVideoBitrate 两个参数，用于约束 SDK 对视频码率的调整范围：
  /// - 如果您将 videoBitrate 和 minVideoBitrate 设置为同一个值，等价于关闭 SDK 对视频码率的自适应调节能力。
  int videoBitrate;

  ///【字段含义】最低视频码率，SDK 会在网络不佳的情况下主动降低视频码率以保持流畅度，最低会降至 minVideoBitrate 所设定的数值。
  ///【推荐取值】您可以通过同时设置 videoBitrate 和 minVideoBitrate 两个参数，用于约束 SDK 对视频码率的调整范围：
  /// - 如果您将 videoBitrate 和 minVideoBitrate 设置为同一个值，等价于关闭 SDK 对视频码率的自适应调节能力。
  int minVideoBitrate;

  V2TXLiveVideoEncoderParam({
    this.videoResolution =
        V2TXLiveVideoResolution.v2TXLiveVideoResolution960x540,
    this.videoResolutionMode =
        V2TXLiveVideoResolutionMode.v2TXLiveVideoResolutionModePortrait,
    this.videoFps = 15,
    this.videoBitrate = 1500,
    this.minVideoBitrate = 800,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['videoResolution'] = videoResolution.index;
    data['videoResolutionMode'] = videoResolutionMode.index;
    data['videoFps'] = videoFps;
    data['videoBitrate'] = videoBitrate;
    data['minVideoBitrate'] = minVideoBitrate;
    return data;
  }
}

/// 本地视频画面预览镜像类型。
enum V2TXLiveMirrorType {
  /// 系统默认镜像类型，前置摄像头镜像，后置摄像头不镜像
  v2TXLiveMirrorTypeAuto,

  /// 前置摄像头和后置摄像头，都切换为镜像模式
  v2TXLiveMirrorTypeEnable,

  /// 前置摄像头和后置摄像头，都切换为非镜像模式
  v2TXLiveMirrorTypeDisable,
}

/// 视频画面填充模式。
enum V2TXLiveFillMode {
  /// 图像铺满屏幕，超出显示视窗的视频部分将被裁剪，画面显示可能不完整
  v2TXLiveFillModeFill,

  /// 图像长边填满屏幕，短边区域会被填充黑色，画面的内容完整
  v2TXLiveFillModeFit,
}

/// 视频画面顺时针旋转角度。
enum V2TXLiveRotation {
  /// 不旋转
  v2TXLiveRotation0,

  /// 顺时针旋转90度
  v2TXLiveRotation90,

  /// 顺时针旋转180度
  v2TXLiveRotation180,

  /// 顺时针旋转270度
  v2TXLiveRotation270,
}

/// 视频帧的像素格式。
enum V2TXLivePixelFormat {
  /// 未知
  v2TXLivePixelFormatUnknown,

  /// YUV420P I420
  v2TXLivePixelFormatI420,

  /// YUV420SP NV12
  v2TXLivePixelFormatNV12,

  /// BGRA8888
  v2TXLivePixelFormatBGRA32,

  /// OpenGL 2D 纹理
  v2TXLivePixelFormatTexture2D,
}

/// 视频数据包装格式。
enum V2TXLiveBufferType {
  /// 未知
  v2TXLiveBufferTypeUnknown,

  /// DirectBuffer，装载 I420 等 buffer，在 native 层使用
  /// 暂不支持
  v2TXLiveBufferTypeByteBuffer,

  /// byte[]，装载 I420 等 buffer，在 Java 层使用
  v2TXLiveBufferTypeByteArray,

  /// 直接操作纹理 ID，性能最好，画质损失最少
  v2TXLiveBufferTypeTexture,
}

class V2TXLiveVideoFrame {

  /// 【字段含义】视频帧像素格式
  /// 【推荐取值】V2TXLivePixelFormatNV12
  V2TXLivePixelFormat pixelFormat;

  /// 【字段含义】视频数据包装格式
  /// 【推荐取值】V2TXLiveBufferTypePixelBuffer
  V2TXLiveBufferType bufferType;

  /// 【字段含义】bufferType 为 V2TXLiveBufferTypeNSData 时的视频数据
  Uint8List? data;

  /// 【字段含义】视频宽度
  int width;

  /// 【字段含义】视频高度
  int height;

  /// 【字段含义】视频帧的顺时针旋转角度
  V2TXLiveRotation rotation;

  /// 【字段含义】视频纹理ID
  int? textureId;

  V2TXLiveVideoFrame(
      {required this.pixelFormat,
      required this.bufferType,
      required this.width,
      required this.height,
      this.rotation = V2TXLiveRotation.v2TXLiveRotation0,
      this.data,
      this.textureId});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> jsonData = <String, dynamic>{};
    jsonData['pixelFormat'] = pixelFormat;
    jsonData['bufferType'] = bufferType;
    jsonData['width'] = width;
    jsonData['height'] = height;
    jsonData['rotation'] = rotation.index;
    if (data != null) {
      jsonData['data'] = data;
    }
    if (textureId != null) {
      jsonData['textureId'] = textureId;
    }
    return jsonData;
  }
}

/// 声音音质。
enum V2TXLiveAudioQuality {
  /// 语音音质：采样率：16k；单声道；音频码率：16kbps；适合语音通话为主的场景，比如在线会议，语音通话
  v2TXLiveAudioQualitySpeech,

  /// 默认音质：采样率：48k；单声道；音频码率：50kbps；SDK 默认的音频质量，如无特殊需求推荐选择之
  v2TXLiveAudioQualityDefault,

  /// 音乐音质：采样率：48k；双声道 + 全频带；音频码率：128kbps；适合需要高保真传输音乐的场景，比如K歌、音乐直播等
  v2TXLiveAudioQualityMusic,
}

class V2TXLiveAudioFrame {
  /// 【字段含义】音频数据
  Uint8List data;

  /// 【字段含义】采样率
  int sampleRate;

  /// 【字段含义】声道数
  int channel;

  V2TXLiveAudioFrame(
      {required this.data, required this.sampleRate, required this.channel});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> jsonData = <String, dynamic>{};
    jsonData['data'] = data;
    jsonData['sampleRate'] = sampleRate;
    jsonData['channel'] = channel;
    return jsonData;
  }
}

/// 混流输入类型配置
enum V2TXLiveMixInputType {
  /// 混入音视频
  v2TXLiveMixInputTypeAudioVideo,

  /// 只混入视频
  v2TXLiveMixInputTypePureVideo,

  /// 只混入音频
  v2TXLiveMixInputTypePureAudio,
}

/// 云端混流中每一路子画面的位置信息
class V2TXLiveMixStream {

  /// 【字段含义】参与混流的 userId
  String userId;

  /// 【字段含义】参与混流的 userId 所在对应的推流 streamId，nil 表示当前推流 streamId
  String? streamId;

  /// 【字段含义】图层位置 x 坐标（绝对像素值）
  int x;

  /// 【字段含义】图层位置 y 坐标（绝对像素值）
  int y;

  /// 【字段含义】图层位置宽度（绝对像素值）
  int width;

  /// 【字段含义】图层位置高度（绝对像素值）
  int height;

  /// 【字段含义】图层层次（1 - 15）不可重复
  int zOrder;

  /// 【字段含义】该直播流的输入类型
  V2TXLiveMixInputType inputType;

  V2TXLiveMixStream(
      {required this.userId,
        this.x = 0,
        this.y = 0,
        this.width = 360,
        this.height = 640,
        this.zOrder = 1,
        this.inputType = V2TXLiveMixInputType.v2TXLiveMixInputTypeAudioVideo});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userId'] = userId;
    data['streamId'] = streamId ?? "";
    data['x'] = x;
    data['y'] = y;
    data['width'] = width;
    data['height'] = height;
    data['zOrder'] = zOrder;
    data['inputType'] = inputType.index;
    return data;
  }
}

/// 云端混流（转码）配置
class V2TXLiveTranscodingConfig {

  /// 【字段含义】最终转码后的视频分辨率的宽度
  /// 【推荐取值】推荐值：360px，如果你是纯音频推流，请将 width × height 设为 0px × 0px，否则混流后会携带一条画布背景的视频流
  int videoWidth;

  /// 【字段含义】最终转码后的视频分辨率的高度
  /// 【推荐取值】推荐值：640px，如果你是纯音频推流，请将 width × height 设为 0px × 0px，否则混流后会携带一条画布背景的视频流
  int videoHeight;

  /// 【字段含义】最终转码后的视频分辨率的码率（kbps）
  /// 【推荐取值】如果填0，后台会根据 videoWidth 和 videoHeight 来估算码率，您也可以参考枚举定义 V2TXLiveVideoResolution 的注释
  int videoBitrate;

  /// 【字段含义】最终转码后的视频分辨率的帧率（FPS）
  /// 【推荐取值】默认值：15fps，取值范围是 (0,30]
  int videoFramerate;

  /// 【字段含义】最终转码后的视频分辨率的关键帧间隔（又称为 GOP）
  /// 【推荐取值】默认值：2，单位为秒，取值范围是 [1,8]
  int videoGOP;

  /// 【字段含义】混合后画面的底色颜色，默认为黑色，格式为十六进制数字，比如：“0x61B9F1” 代表 RGB 分别为(97,158,241)
  /// 【推荐取值】默认值：0x000000，黑色
  int backgroundColor;

  /// 【字段含义】混合后画面的背景图
  /// 【推荐取值】默认值：nil，即不设置背景图
  /// 【特别说明】背景图需要您事先在 “[控制台](https://console.cloud.tencent.com/trtc) => 应用管理 => 功能配置 => 素材管理” 中上传，
  ///            上传成功后可以获得对应的“图片ID”，然后将“图片ID”转换成字符串类型并设置到 backgroundImage 里即可。
  ///            例如：假设“图片ID” 为 63，可以设置 backgroundImage = "63";
  String? backgroundImage;

  /// 【字段含义】最终转码后的音频采样率
  /// 【推荐取值】默认值：48000Hz。支持12000HZ、16000HZ、22050HZ、24000HZ、32000HZ、44100HZ、48000HZ
  int audioSampleRate;

  /// 【字段含义】最终转码后的音频码率
  /// 【推荐取值】默认值：64kbps，取值范围是 [32，192]，单位：kbps
  int audioBitrate;

  /// 【字段含义】最终转码后的音频声道数
  /// 【推荐取值】默认值：1。取值范围为 [1,2] 中的整型
  int audioChannels;

  /// 【字段含义】每一路子画面的位置信息
  List<V2TXLiveMixStream> mixStreams = <V2TXLiveMixStream>[];

  /// 【字段含义】输出到 CDN 上的直播流 ID
  ///          如不设置该参数，SDK 会执行默认逻辑，即房间里的多路流会混合到该接口调用者的视频流上，也就是 A + B => A；
  ///          如果设置该参数，SDK 会将房间里的多路流混合到您指定的直播流 ID 上，也就是 A + B => C。
  /// 【推荐取值】默认值：nil，即房间里的多路流会混合到该接口调用者的视频流上。
  String? outputStreamId;

  V2TXLiveTranscodingConfig(
      {this.videoWidth = 360,
      this.videoHeight = 640,
      this.videoBitrate = 0,
      this.videoFramerate = 15,
      this.videoGOP = 2,
      this.backgroundColor = 0x000000,
      this.audioSampleRate = 48000,
      this.audioBitrate = 64,
      this.audioChannels = 1});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['videoWidth'] = videoWidth;
    data['videoHeight'] = videoHeight;
    data['videoBitrate'] = videoBitrate;
    data['videoFramerate'] = videoFramerate;
    data['videoGOP'] = videoGOP;
    data['backgroundColor'] = backgroundColor;
    if (backgroundImage != null) {
      data['backgroundImage'] = backgroundImage ?? "";
    }
    data['audioSampleRate'] = audioSampleRate;
    data['audioBitrate'] = audioBitrate;
    data['audioChannels'] = audioChannels;
    if (outputStreamId != null) {
      data['outputStreamId'] = outputStreamId ?? "";
    }
    if (mixStreams.isNotEmpty) {
      List<Map> mixStreamsDatas = <Map>[];
      for (var item in mixStreams) {
        mixStreamsDatas.add(item.toJson());
      }
      data['mixStreams'] = mixStreamsDatas;
    }
    return data;
  }

}

/// 日志级别枚举值
enum V2TXLiveLogLevel {
  /// 输出所有级别的 log
  v2TXLiveLogLevelAll,

  /// 输出 DEBUG，INFO，WARNING，ERROR 和 FATAL 级别的 log
  v2TXLiveLogLevelDebug,

  /// 输出 INFO，WARNING，ERROR 和 FATAL 级别的 log
  v2TXLiveLogLevelInfo,

  /// 只输出 WARNING，ERROR 和 FATAL 级别的 log
  V2TXLiveLogLevelWarning,

  /// 只输出 ERROR 和 FATAL 级别的 log
  v2TXLiveLogLevelError,

  /// 只输出 FATAL 级别的 log
  v2TXLiveLogLevelFatal,

  /// 不输出任何 sdk log
  v2TXLiveLogLevelNULL,
}

class V2TXLiveLogConfig {

  /// 【字段含义】设置 Log 级别
  /// 【推荐取值】默认值：V2TXLiveLogLevelAll
  V2TXLiveLogLevel logLevel;

  /// 【字段含义】是否通过 V2TXLivePremierObserver 接收要打印的 Log 信息
  /// 【特殊说明】如果您希望自己实现 Log 写入，可以打开此开关，Log 信息会通过 V2TXLivePremierObserver#onLog 回调给您。
  /// 【推荐取值】默认值：false
  bool enableObserver;

  /// 【字段含义】是否允许 SDK 在编辑器（XCoder、Android Studio、Visual Studio 等）的控制台上打印 Log
  /// 【推荐取值】默认值：false
  bool enableConsole;

  /// 【字段含义】是否启用本地 Log 文件
  /// 【特殊说明】如非特殊需要，请不要关闭本地 Log 文件，否则腾讯云技术团队将无法在出现问题时进行跟踪和定位。
  /// 【推荐取值】默认值：true
  bool enableLogFile;

  /// 【字段含义】设置本地 Log 的存储目录，默认 Log 存储位置：
  ///  iOS & Mac: sandbox Documents/log
  ///  Android
  ///   * 6.7 or below: /sdcard/log/liteav
  ///   * 6.8 or above: /sdcard/Android/data/package name/files/log/liteav/
  String? logPath;

  V2TXLiveLogConfig(
      {this.logLevel = V2TXLiveLogLevel.v2TXLiveLogLevelAll,
        this.enableObserver = false,
        this.enableConsole = false,
        this.enableLogFile = true});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['logLevel'] = logLevel.index;
    data['enableObserver'] = enableObserver;
    data['enableConsole'] = enableConsole;
    data['enableLogFile'] = enableLogFile;
    if (logPath != null) {
      data['logPath'] = logPath;
    }
    return data;
  }
}