
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'v2_tx_live_code.dart';
import 'v2_tx_live_def.dart';

enum V2TXLivePremierObserverType {
  /// 自定义 Log 输出回调接口
  onLog,
  /// setLicence 接口回调
  /// @param reason 设置 licence 失败原因
  /// @param result 设置 licence 结果 0 成功，负数失败
  onLicenceLoaded,
}

typedef V2TXLivePremierObserver<P> = void Function(V2TXLivePremierObserverType type, P? params);

class V2TXLivePremier {

  V2TXLivePremier._privateConstructor();

  static final V2TXLivePremier _instance = V2TXLivePremier._privateConstructor();
  late MethodChannel _channel;
  late V2TXLivePremierObserver? _observer;

  factory V2TXLivePremier(){
    _instance._initMethodChannel();
    return _instance;
  }

  _initMethodChannel() {
    _channel = const MethodChannel("live_cloud_premier");
    _channel.setMethodCallHandler((MethodCall call) async {
      debugPrint("MethodCallHandler method: ${call.method}");
      var arguments = call.arguments;
      if (arguments is Map) {
        debugPrint("arguments is Map: $arguments");
      } else {
        debugPrint("arguments isn't Map: $arguments");
      }
      if (call.method == "onPremierListener") {
        String typeStr = arguments['type'];
        var params = arguments['params'];
        debugPrint("on Premier Listener: type:$typeStr");
        V2TXLivePremierObserverType? callType;
        for (var subType in V2TXLivePremierObserverType.values) {
          if (subType.toString().replaceFirst("V2TXLivePremierObserverType.", "") == typeStr) {
            callType = subType;
            break;
          }
        }
        if (callType != null && _observer != null) {
          _observer!(callType, params);
        }
      } else {
        debugPrint("on Player Listener: MethodNotImplemented ${call.method}");
      }
    });
  }

  /// 设置 SDK 的授权 License
  /// 文档地址：https://cloud.tencent.com/document/product/454/34750
  /// @param url licence的地址
  /// @param key licence的秘钥
  static Future<void> setLicence(String url, String key) async {
    await V2TXLivePremier()._channel.invokeMethod("setLicence", {"url": url, "key": key});
  }

  /// 获取SDK版本信息
  static Future<String> getSDKVersionStr() async {
    var version = await V2TXLivePremier()._channel.invokeMethod("getSDKVersionStr");
    return version;
  }

  /// 设置 V2TXLivePremier 回调接口
  static Future<void> setObserver(V2TXLivePremierObserver? observer) async {
    V2TXLivePremier()._observer = observer;
  }

  /// 设置 Log 的配置信息
  static Future<V2TXLiveCode> setLogConfig(V2TXLiveLogConfig config) async {
    var code = await V2TXLivePremier()._channel.invokeMethod("setLogConfig", {"config": config.toJson()});
    if (code is V2TXLiveCode) {
      return code;
    } else {
      return V2TXLIVE_ERROR_FAILED;
    }
  }

  /// 设置 SDK 接入环境
  ///
  /// @note 如您的应用无特殊需求，请不要调用此接口进行设置。
  /// @param env 目前支持 “default” 和 “GDPR” 两个参数
  /// - default：默认环境，SDK 会在全球寻找最佳接入点进行接入。
  /// - GDPR：所有音视频数据和质量统计数据都不会经过中国大陆地区的服务器。
  static Future<V2TXLiveCode> setEnvironment(String env) async {
    var code = await V2TXLivePremier()._channel.invokeMethod("setEnvironment", {"env": env});
    if (code is V2TXLiveCode) {
      return code;
    } else {
      return V2TXLIVE_ERROR_FAILED;
    }
  }

  /// 设置 SDK sock5 代理配置
  ///
  /// @param host sock5 代理服务器的地址
  /// @param port sock5 代理服务器的端口
  /// @param username sock5 代理服务器的验证的用户名
  /// @param password sock5 代理服务器的验证的密码
  static Future<V2TXLiveCode> setSocks5Proxy(
      String host, int port, String username, String password) async {
    var code = await V2TXLivePremier()._channel.invokeMethod("setSocks5Proxy", {
      "host": host,
      "port": port,
      "username": username,
      "password": password
    });
    if (code is V2TXLiveCode) {
      return code;
    } else {
      return V2TXLIVE_ERROR_FAILED;
    }
  }
}