

///
/// 腾讯云直播的播放器回调通知。<br/>
/// 可以接收 V2TXLivePlayer 播放器的一些回调通知，包括播放器状态、播放音量回调、音视频首帧回调、统计数据、警告和错误信息等。
///
enum V2TXLivePlayerListenerType {
  /// 错误回调，表示 SDK 不可恢复的错误，一定要监听并分情况给用户适当的界面提示
  onError,

  /// 警告回调，用于告知您一些非严重性问题，例如出现卡顿或者可恢复的解码失败。
  onWarning,

  /// 直播播放器分辨率变化通知
  onVideoResolutionChanged,

  /// 已经成功连接到服务器
  onConnected,

  /// 音频播放事件
  onAudioPlaying,

  /// 视频加载事件
  onVideoLoading,

  /// 音频加载事件
  onAudioLoading,

  /// 播放器音量大小
  onPlayoutVolumeUpdate,

  /// 直播播放器统计数据回调
  onStatisticsUpdate,

  /// 截图回调
  onSnapshotComplete,

  /// 自定义视频渲染回调
  onRenderVideoFrame,

  /// 收到 SEI 消息的回调
  onReceiveSeiMessage,
}

typedef V2TXLivePlayerObserver<P> = void Function(V2TXLivePlayerListenerType type, P? params);
